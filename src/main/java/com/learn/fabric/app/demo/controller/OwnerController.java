package com.learn.fabric.app.demo.controller;

import com.google.common.collect.Maps;
import com.learn.fabric.app.demo.config.HyperLedgerFabricProperties;
import com.learn.fabric.app.demo.dto.OwnerDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.StringUtils;


import org.hyperledger.fabric.gateway.Contract;
import org.hyperledger.fabric.gateway.Gateway;
import org.hyperledger.fabric.gateway.GatewayException;
import org.hyperledger.fabric.gateway.Network;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.Map;


@RestController
@RequestMapping("/owner")
@Slf4j
@AllArgsConstructor
public class OwnerController {
    final Gateway gateway;


    final Network network;

    private static Contract ownerContract;

    final HyperLedgerFabricProperties hyperLedgerFabricProperties;


    @PostConstruct
    public void init()
    {
        ownerContract = network.getContract("hyperledger-fabric-contract-java-demo-learnCat","OwnerContract");
        log.info("ownerContract初始化成功！");
    }
    /**
     * description: 根据key查询
     * @param key
     * @return
     * @throws GatewayException
     */
    @GetMapping("/{key}")
    public Map<String, Object> queryOwnerByKey(@PathVariable String key) throws GatewayException {

        Map<String, Object> result = Maps.newConcurrentMap();
        byte[] Owner = ownerContract.evaluateTransaction("queryOwner", key);

        result.put("payload", StringUtils.newStringUtf8(Owner));
        result.put("status", "ok");

        return result;
    }

    /**
     * description：新增Owner
     * @param Owner
     * @return
     * @throws Exception
     */
    @PutMapping("/")
    public Map<String, Object> createOwner(@RequestBody OwnerDTO Owner) throws Exception {

        Map<String, Object> result = Maps.newConcurrentMap();


        byte[] bytes = ownerContract.submitTransaction("createOwner", Owner.getId(), Owner.getName());

        result.put("payload", StringUtils.newStringUtf8(bytes));
        result.put("status", "ok");
        return result;
    }

    /**
     * description: 更新Owner
     * @param Owner
     * @return
     * @throws Exception
     */
    @PostMapping("/")
    public Map<String, Object> updateOwner(@RequestBody OwnerDTO Owner) throws Exception {

        Map<String, Object> result = Maps.newConcurrentMap();
        byte[] bytes = ownerContract.submitTransaction("updateOwner",Owner.getId(), Owner.getName(),Owner.getGoodsName());

        result.put("payload", StringUtils.newStringUtf8(bytes));
        result.put("status", "ok");

        return result;
    }

    /**
     * description: 根据key删除Owner
     * @param key
     * @return
     * @throws Exception
     */
    @DeleteMapping("/{key}")
    public Map<String, Object> deleteOwnerByKey(@PathVariable String key) throws Exception {

        Map<String, Object> result = Maps.newConcurrentMap();

        byte[] Owner = ownerContract.submitTransaction("deleteOwner" , key);

        result.put("payload", StringUtils.newStringUtf8(Owner));
        result.put("status", "ok");

        return result;
    }
}
