package com.learn.fabric.app.demo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class OwnerDTO {
    private String  id;

    private String name;

    private String GoodsName;
}
