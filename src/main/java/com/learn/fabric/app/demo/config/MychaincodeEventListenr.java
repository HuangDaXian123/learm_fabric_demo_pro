package com.learn.fabric.app.demo.config;

import org.hyperledger.fabric.sdk.BlockEvent;
import org.hyperledger.fabric.sdk.ChaincodeEvent;
import org.hyperledger.fabric.sdk.ChaincodeEventListener;
import org.springframework.context.annotation.Bean;


public class MychaincodeEventListenr implements ChaincodeEventListener {
    @Override
    public void received(String s, BlockEvent blockEvent, ChaincodeEvent chaincodeEvent) {
        System.out.println("Received event: " + chaincodeEvent.getEventName());
        System.out.println("Payload: " + new String(chaincodeEvent.getPayload()));
    }
}
