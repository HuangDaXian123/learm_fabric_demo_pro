package com.learn.fabric.app.demo.dto;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CatDTO {

    String key;

    String name;

    Integer age;

    String color;

    String breed;
}
