package com.learn.fabric.app.demo.controller;

import com.google.common.collect.Maps;
import com.learn.fabric.app.demo.config.HyperLedgerFabricProperties;
import com.learn.fabric.app.demo.dto.GoodsDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.StringUtils;

import org.hyperledger.fabric.gateway.Contract;
import org.hyperledger.fabric.gateway.Gateway;
import org.hyperledger.fabric.gateway.GatewayException;
import org.hyperledger.fabric.gateway.Network;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.Map;

@RestController
@RequestMapping("/goods")
@Slf4j
@AllArgsConstructor
public class GoodsController {
    
    final Gateway gateway;

    final Network network;

    private static Contract goodsContract;

    final HyperLedgerFabricProperties hyperLedgerFabricProperties;


    @PostConstruct
    public void init()
    {
        goodsContract = network.getContract("hyperledger-fabric-contract-java-demo-learnCat","GoodsContract");
        log.info("goodsContract初始化成功！");
    }
    /**
     * description: 根据key查询
     * @param key
     * @return
     * @throws GatewayException
     */
    @GetMapping("/{key}")
    public Map<String, Object> queryGoodsByKey(@PathVariable String key) throws GatewayException {

        Map<String, Object> result = Maps.newConcurrentMap();
        byte[] goods = goodsContract.evaluateTransaction("queryGoods", key);

        result.put("payload", StringUtils.newStringUtf8(goods));
        result.put("status", "ok");

        return result;
    }

    /**
     * description：新增Goods
     * @param Goods
     * @return
     * @throws Exception
     */
    @PutMapping("/")
    public Map<String, Object> createGoods(@RequestBody GoodsDTO Goods) throws Exception {

        Map<String, Object> result = Maps.newConcurrentMap();


        byte[] bytes = goodsContract.submitTransaction("createGoods", Goods.getId(), Goods.getName(), Goods.getState());

        result.put("payload", StringUtils.newStringUtf8(bytes));
        result.put("status", "ok");
        return result;
    }

    /**
     * description: 更新Goods
     * @param Goods
     * @return
     * @throws Exception
     */
    @PostMapping("/")
    public Map<String, Object> updateGoods(@RequestBody GoodsDTO Goods) throws Exception {

        Map<String, Object> result = Maps.newConcurrentMap();
        byte[] bytes = goodsContract.submitTransaction("updateGoods", Goods.getId(), Goods.getName(), Goods.getState());

        result.put("payload", StringUtils.newStringUtf8(bytes));
        result.put("status", "ok");

        return result;
    }

    /**
     * description: 根据key删除Goods
     * @param key
     * @return
     * @throws Exception
     */
    @DeleteMapping("/{key}")
    public Map<String, Object> deleteGoodsByKey(@PathVariable String key) throws Exception {

        Map<String, Object> result = Maps.newConcurrentMap();

        byte[] Goods = goodsContract.submitTransaction("deleteGoods" , key);

        result.put("payload", StringUtils.newStringUtf8(Goods));
        result.put("status", "ok");

        return result;
    }
}
